# Register your new SOLID application
Initialy you have to do a 'Dynamic Client Registration' to get a client_id and a client_secret for you app:
```
curl --location --request POST 'https://solidcommunity.net/register' \  
--header 'Content-Type: application/json' \  
--data-raw '{  
"application_type": "native",  
"redirect_uris": ["http://localhost:8080/oidc_callback"],  
"client_name": "My Example App",  
"subject_type": "pairwise"  
}'  
```
Sample response:  
```  
{  
    "redirect_uris": [  
        "http://localhost:8080/oidc_callback"  
    ],  
    "client_id": "c3514904fe7xxxxxxxxxxxxxx",  
    "client_secret": "57bcefefdd500e35xxxxxxxxxxx",  
    "response_types": [  
        "code"  
    ],
    "grant_types": [
        "authorization_code"
    ],
    "application_type": "native",
    "client_name": "My Example App",
    "subject_type": "pairwise",
    "id_token_signed_response_alg": "RS256",
    "token_endpoint_auth_method": "client_secret_basic",
    "registration_access_token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "registration_client_uri": "https://solidcommunity.net/register/c3514904fxxxxxxxxxxxxxxxxxxxxxxx",
    "client_id_issued_at": 1603564032,
    "client_secret_expires_at": 0
}
```

# Create an web application that sends a HTTP request:
```
<!DOCTYPE html>
<head>
<title>auth</title>
</head>

<body>
  <form action="https://solidcommunity.net/authorize" method="get">
    <label>response_type:</label> <input type="text" value="code" name="response_type" ><br>
    <br> 
    <label>client_id:</label> <input type="text" value="xxxxxxxxxxxxxxxxxxxxxxx" name="client_id"><br>
    <br> 
    <label>sope:</label> <input type="text" value="openid" name="scope"><br>
    <br>
    <label>redirect_uri:</label> <input type="text" value="http://localhost:8080/oidc_callback" name="redirect_uri"><br>
    <br>
    <input type="submit" value="Submit">
  </form>
</body>
</html>
```
Therefore use the client_id from the registration above.

The redirect url, that has also to be used while registration! Is called and a code is attached.

http://localhost:8080/oidc_callback?code=705ee71ebcbe437a8b5d2bba87e13d1b  

# Request the access-token
The code sends to the redirect_uri can be used to get an access token:  
```
curl --location --request POST 'https://solidcommunity.net/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'grant_type=authorization_code' \
--data-urlencode 'client_id=c3514904fe7acb08dd8e4a3b8181cb04' \
--data-urlencode 'code=705ee71ebcbe437a8b5d2bba87e13d1b' \
--data-urlencode 'client_secret=57bcefefdd500e35509f77ba6ba3dfa7' \
--data-urlencode 'redirect_uri=http://localhost:8080/oidc_callback'
```

Sample Response:  
```
{
    "access_token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "token_type": "Bearer",
    "expires_in": 1209600,
    "refresh_token": "xxxxxxxxxxxxxxxxxxxxxx",
    "id_token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}
```
